<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\db\Connection;




class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        /**
         * voy a hacer consultas a lo loco
         * -utilizar la libreria mysqli (php)
         * -utilizar Yii\db\Command
         */
        
        /*
         * voy a crear una conexion
         */
        
        $db = new Connection([
            'dsn' => 'mysql:host=localhost;dbname=ejemplo1Yii2',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);
        
        /**
         * Ejecutamos una consulta utilizando esa conexion
         * Para ello necesitamos la clase Comando
         */
        
        $comando=$db->createCommand("Select * from Noticias"); 
        $salida=$comando->queryAll();
        
        
        /**
         * Cada vez que quieras llegar a la aplicacion (Clase yii\web\Application()) debeis utilizar Yii::$app 
         * Podemos utilizar la conexion que hemos configurado en la aplicacion Yii::$app->db
        */
        
        $comando1=Yii::$app->db->createCommand("Select * from noticias");
        $salida1=$comando1->queryAll();
        
        
        
        
        /**
         * Otra manera de tratar con bases de datos puede ser el uso de Constructores de Consultas (Query Builder). 
         * El Constructor de Consultas proporciona un medio orientado a objetos para generar las consultas que se ejecutarán.
         * Eso es a traves de la clase yii\db\Query
         */
        
        $consulta=new \yii\db\Query();
        
        $salida2=$consulta->select('*')
                ->from('noticias')
                ->all();
        
        
        /**
         * La mejor forma de realizar consultas (sobre todo las consultas tipicas)
         * es mediante Active Record
         * Para ello vamos a utilizar la clase yii/db/ActiveRecord
         * Ademas tenemos que modelar todas las tablas
         * para realizar los modelos utilizamos Gii
         * 
         */
        
        $consulta= \app\models\Noticias::find();
        
        /**
         * Debido a que yii\db\ActiveQuery se extiende desde yii\db\Query , puede usar todos los métodos de creación de consultas y métodos de consulta del padre
         */
        
        $salidaArrayObjetos=$consulta->all();
        $salida3=$consulta->asArray()->all();

        return $this->render('index',[
            "s"=>$salida,
            "s1"=>$salida1,
            "s2"=>$salida2,
            "s3"=>$salida3,
            "so"=>$salidaArrayObjetos,
        ]);
    }

}
